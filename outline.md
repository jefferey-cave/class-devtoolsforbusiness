# Outline

The book is laid out in chapters making it suitable for a standard post-secondary semester (12?)


## Pattern

1. Explanation
1. Activity
1. Lab
1. Extended learning
	1. Theory
	1. History


## Learning Objectives

Chapters on content are the core of the knowledge, while things around styling are exta knowledge.



- Paradigm shift
  - away from deskop publishing
  - away from WYSIWIG
- History
  - O'Reilly books
  - User Manuals
  - Public Standards (W3C) -- away from Word/PDF/Paper
    - Accessible
	- Electronic layout
- Specific Tools (hello world)
  - Gitlab
  - VS Code
  - Sphinx
  - Markdown
- Processes
  - Change Management
  - Distributed Teams
  - Transparency
  - Domains of expertise (designers vs writers)
  - Audit Logs/Change Logs


## Lessons

1. Tools (hello world)
   - how the book will proceed
1. Gitlab
1. Version Control
	1. Tagging
1. MarkDown
1. Request for Change
1. Branching/Merging
1. Merge Requests
1. Publishing (Sphinx)
1. Multiple Outputs
   - epub/html/paper

> Content is King

I've currently tried to lay it out lay out the chapters usch that a student who had a grasp of the first 9-10 chapters has mastered te concepts. Chapters 11 and 12 are very much advanced usage.

The Chapter on Translations ... I'm not sure is "advanced". It is a very useful thing to understand, however it may


## Labs

Three lab activities that are independant themes

  - Data Lake
  - Policy
  - User Manual for Software
