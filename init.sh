python -m venv .venv;
source .venv/bin/activate;

apt-get update;
#apt-get install -y latexmk texlive-full;
apt-get install -y tzdata;

python -m pip install sphinx myst-parser sphinxcontrib-mermaid;
