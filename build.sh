#!/bin/bash


# https://www.sphinx-doc.org/en/master/man/sphinx-build.html

source .venv/bin/activate;
sphinx-build --version;


# You can set these variables from the command line, and also
# from the environment for the first two.
SPHINXOPTS=;
SPHINXBUILD=sphinx-build;
SOURCEDIR=content;
BUILDDIR=build;
OUTDIR=public;

FILENAME=DevToolsForBusiness


function build {
	$SPHINXBUILD -M $1 $SOURCEDIR $BUILDDIR $SPHINXOPTS -c ./ -a;
}

function build.html {
	build html;
	build singlehtml;

	cp -a $BUILDDIR/html $OUTDIR/${FILENAME}
	cp $BUILDDIR/singlehtml/index.html $OUTDIR/${FILENAME}/singlehtml.html;

	rm -rf $OUTDIR/html/_sources;

	cat  <<- EOF > $OUTDIR/index.html
		<html>
		<head>
		<meta http-equiv="Refresh" content="0; url='./${FILENAME}/index.html'" />
		</head>
		</html>
	EOF
}

function build.epub {
	build epub;
	cp $BUILDDIR/epub/*.epub $OUTDIR/${FILENAME}.epub;
}

function build.pdf {
	build latexpdf;
	cp $BUILDDIR/latex/*.pdf $OUTDIR/${FILENAME}.pdf;
}

function help {
	runner help;
}


function main {
	pushd .

	mkdir -p $OUTDIR;
	rm -rf $OUTDIR/*;
	rm -rf $BUILDDIR/*;

	build.html;
	build.epub;

	#build.pdf;
	#build qthelp;
	#build devhelp;
	#build gettext;

	popd;
}


main;
