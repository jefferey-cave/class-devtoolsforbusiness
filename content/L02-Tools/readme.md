# The Tools

```{contents}
:depth: 3
```

Over the past 20 years, a number of tools have been developed to aid in the development of virtual products or documents. These tools have revolutionized the way teams collaborate, manage changes, and streamline the publishing process. In this chapter, we will explore some of the key tools that are essential for undergraduate students engaged in virtual product or document development.


**Key Concepts**

- 4 types of tool
  - Central Repository
  - Version Control Client
  - Document Editor
- Simpler tools are better than visual tools


## Best Tool

One of the classic arguments in software development revolves around which programming language is the best. However, there is a secret that all developers eventually come to realize: no programming language is inherently better than others. The superiority of a tool lies not in the language itself but in how it is utilized. Familiarity with a tool does not automatically make it superior to others. Therefore, it is crucial to understand that the value of a tool lies in what you can accomplish with it.

I know, I promised not to get technical, but there is a point to this:

> its not about the tools, but rather what you do with them.

Being familiar with a tool is not proof of that tool's superiority.

While some tools may be easier to learn or have better cost-value structures, it is important to consider the specific problem at hand and the implementation of the tools being used. Merely being familiar with a tool may overshadow the benefits of alternative tools, and hinder your ability to explore new toolsets. Therefore, instead of searching for the "best" tool, focus on finding the tool that best suits your needs and complements your workflow.

In the context of virtual product or document development, I will argue that version control tools, developed based on years of experience in managing distributed teams, are battle-hardened tools that provide significant advantages. However, it is essential to note that the choice of a specific tool within your organization's ecosystem is subjective and may vary based on corporate structure and policies.

## What Tools

There are several tools on the market that will assist in managing the changes associated with long term management of a document.

### Central Repository

A central repository serves as the foundation for collaborative development and provides visibility into changes and activities across the entire team. Here are three popular central repository options:

- Microsoft Azure DevOps: Azure DevOps offers a comprehensive suite of tools for project management, version control, and continuous integration/continuous delivery (CI/CD). It provides a centralized platform for collaboration and tracking changes.

- GitLab: GitLab is a web-based Git repository manager that offers a complete DevOps platform. It provides a centralized repository with integrated tools for version control, issue tracking, and CI/CD pipelines.

- Microsoft GitHub: GitHub is a web-based hosting service for Git repositories. It provides a collaborative platform for version control, issue tracking, and project management. GitHub is widely adopted by open-source projects and offers extensive community support.

There are many others, but these three are relatively ubiquitous and dominate the current market.

Whichever tool you pick, these three offer a centralized repository that allows for the entire team to have visibility into the changes and activity being undertaken.

They act as a way to gather together the major components and tie them together.

1. Version Control
2. Issue Reporting and Change Requests
3. Documenting Rules and Guidance

When selecting a tool for your corporate environment, there are several tings to consider:

When selecting a tool for your corporate environment, consider the following factors:

1. Existing tool usage: Check with your development team or IT department to identify if any of these tools are already in use within your organization. Leveraging existing tools can be more cost-effective and simplify the adoption process.

2. Cost effectiveness: Evaluate the cost structures of different tools. GitHub and GitLab offer free options for individuals, while Azure DevOps provides comprehensive enterprise-level solutions.

3. Security considerations: Ensure that the chosen tool aligns with your organization's security requirements and data protection policies.

4. Support and community: Consider the availability of support resources, documentation, and a vibrant user community for the selected tool. Active communities can provide valuable insights and assistance when needed.

### Version Control Client

Since 1980 (???) version control has been a significant issue to be managed. Several tools have been developed over the years, representing refinements in the way documents are managed.

- Revision Control System (RCS)
- Concurrent Version System (CVS)
- Subversion (Svn)
- Git

The most common version control tool on the market currently is Git. Git has been developed as a way to track changes to a document as well as a way to share those changes across many individuals.

Git's distributed nature is both a strength and a weakness. It was specificially designed to protect open source projects from being stolen or shutdown, it was meant to operate in uncontrolled environments. Everyone with a copy has a copy of all the changes (the entire database).

One of the significant advantages of tools such as this is that they allow us to work on our local computer while later transmitting these changes to a central location for storage. For those of us working from home, this has been a huge boon: we are able to work remotely, using the internet to share our work with our colleagues, but also to continue working in the event of a shaky internet connection, or even a complete internet failure.

> **EXAMPLE**
>
> In 2018, due to the pandemic restrictions, I like many others, was barred
> from leaving my home. This was alright, as I was in a comfortable
> apartment with a great view ofthe Halifax Harbour. While sitting in
> my home office, working on a report to share with my manager, I would
> periodically look out hte window to watch a giant shipping vessel parked
> outside... at anchor, slowly swaying in the wind and the outgoing tide.
>
> and then it moved... a lot...
>
> and then I lost my VPN
>
> According to my internet provider, an underwater cable had been cut
> in the Halifax harbour and everyone on one side of the harbour (my side)
> were without internet until a new cable could be patched and laid.
>
> Accidents happen.

On an airplane, a beach, or a shipping vessel, we can use version control
systems, to work locally, save our work, and share it when we get a connection
later. This has the extra benefit of ensuring we are happy with our work
before we share. Lastly tools like this are specifically designed to clearly
identify the changes between documents which is useful as we attempt to
decide whether changes are healthy or not.

Git was designed as a central repository, and therefore we need a "client"
to connect to it. A piece of software meant to be used on your computer
that will allow you to control the changes you make and send them to the
central repository.

Common tools are

- TortoiseGit: TortoiseGit is a Windows-based Git client that integrates with the Windows shell. It provides a user-friendly interface for managing Git repositories and performing version control operations.

- RapidGit: RapidGit is a cross-platform Git client that offers a streamlined and intuitive user interface. It simplifies Git operations and provides visualizations to understand changes easily.

- GitKraken: GitKraken is a popular Git client known for its visually appealing and feature-rich interface. It offers advanced features such as drag-and-drop branching, conflict resolution, and seamless integration with Git repositories.

While these tools provide similar core functionalities, their user interfaces and additional features may differ. Experiment with different Git clients to find the one that aligns with your preferences and enhances your productivity.


### Document Editor

The adoption of word processors (Word Perfect, and Word) introduced a new way to become involved in home publishing, and the graphical nature of the  tools allowed peopel to see what they were going to get before they actually got it.

Since 1995 Microsoft Word has been the most pervasive document editor in the world.

As with so many tools, their strengths are also their weaknesses.

In a move toward a more holistic publishing chain, O'Reilly Books (a publisher specializing in books about computing) moved toward a text-based standard. This put much of the responsibility, but also control, in the hands of authors themselves. It also had the advantage of taking away some of the decision making. Authors are responsible for authoring, typesetting will be handeled by someone else. Therefore getting distracted with changing a font is a huge distraction to an author.

However, for a more holistic publishing approach, many have transitioned towards text-based authoring.

Text-based authoring offers several advantages, including:

Limited palette: By focusing on expressing ideas rather than formatting, authors can concentrate on content creation.
Semantics and separation of concerns: Text-based authoring emphasizes marking elements based on their semantic meaning rather than specific formatting instructions. This allows for clear communication of intent, which can be leveraged by typesetting professionals during the publishing process.
Change visualization: Removing formatting distractions allows for a clearer view of meaningful changes during editorial processes.
While a simple text editor is sufficient for text-based authoring, additional features can enhance the authoring experience. Here are a couple of commonly used text editors:

Notepad++: Notepad++ is a lightweight and versatile text editor for Windows. It supports various programming languages, provides syntax highlighting, and allows for efficient text manipulation.

Visual Studio Code: Visual Studio Code (VS Code) is a popular cross-platform text editor developed by Microsoft. It offers a wide range of extensions, including spell checkers, grammar checkers, and integrations with other development tools.

Both Notepad++ and Visual Studio Code provide a conducive environment for text-based authoring, enabling authors to focus on content while still benefiting from modern text editing features.

## Conclusion
In this chapter, we explored essential tools for undergraduate students engaged in virtual product or document development. We discussed the significance of selecting tools based on their suitability for the task at hand rather than searching for a universally "best" tool. We explored central repository options, version control clients, and text editors, highlighting popular choices in each category.

By leveraging central repository tools like Azure DevOps, GitLab, or GitHub, teams can collaborate effectively and track changes seamlessly. Version control clients such as TortoiseGit, RapidGit, and GitKraken provide intuitive interfaces to manage Git repositories and facilitate efficient version control workflows. For text-based authoring, Notepad++ and Visual Studio Code offer lightweight and customizable environments, empowering authors to focus on content creation while still enjoying essential text editing features.

As an undergraduate student, understanding and utilizing these tools will equip you with the necessary skills to excel in virtual product or document development. Remember, the key lies not in the tools themselves but in how you leverage them to unleash your creativity and achieve your goals.

## Terminology

- Central Repository

  A centralized location where project files and changes are stored, allowing teams to collaborate and track modifications efficiently.

- Version Control

  The management of changes made to a file or set of files over time, enabling easy tracking, collaboration, and reverting to previous states if necessary.

- Text-based Authoring: A method of document creation that focuses on content rather than formatting, utilizing plain text files marked up with semantic annotations.

- Git

  A distributed version control system widely used for tracking changes in source code and other text-based files.

- Word Processor

  A software application designed for creating, editing, and formatting text-based documents.

- Text Editor

  A software tool used for creating and editing plain text files, offering features such as syntax highlighting, search and replace, and text manipulation capabilities.


While I would personally argue that some tools are easier to follow than others, and some have better cost value structures than others, some of that argument stems from the particular problem I am trying to solve, the particular implementation of the tools we are using, but I cannot emphasize enough: being familiar with a tool does not make it superior to other tools. In fact, your familiarity may be masking benefits of other tools... it may also be that you aren't

## Next Steps

```{toctree}
:maxdepth: 2
:caption: Contents

activity.md
quiz.md
extended.md
lab.md
```
