# Activity - Explore Tools



## Our Tool

While I would personally argue that some tools are easier to follow than
others, and some have better cost-value structures than others, some of
that argument stems from the particular problem I am trying to solve,
the particular implementation of the tools we are using, but I cannot
emphasize enough: being familiar with a tool does not make it superior
to other tools. In fact, your familiarity may be masking the benefits
of other tools... it may also be that you aren't being open to continous
learning.
