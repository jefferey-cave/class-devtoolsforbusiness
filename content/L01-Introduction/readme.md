# Introduction

```{contents}
:depth: 3
:local: True
```

This chapter explores the utilization of software development practices and tools within the realm of project management, specifically focusing on document management. It examines the historical evolution of project management practices in the software industry and identifies key practices that have proven to be successful. The chapter emphasizes the need for knowledge sharing across industries and highlights the benefits of adopting software development practices in project management. Additionally, it discusses the concept of continuous delivery, the role of a steward in maintaining documents, and the importance of continuous improvement. The chapter is aimed at business teams involved in information stewardship, particularly managers and team leads, to provide insights into software development tools and practices that can enhance project management processes and achieve organizational objectives.

In this chapter, we will explore how software development practices and tools can be effectively applied in project management, focusing specifically on document management. We will delve into the historical background of project management practices in the software industry and highlight successful practices that have emerged. By understanding and leveraging these practices, project managers from various industries can enhance their project management processes and achieve better outcomes.



## Software Development Practices in Project Management

### The Evolution of Project Management Practices

   - Discuss the historical development of project management practices in the software industry, emphasizing the emergence of key practices such as version control, approval workflows, deadlines, and quality control.
   - Highlight the collaborative nature of process development in the software industry and the establishment of standardized processes across the field.
   - Draw parallels between software development practices and project management practices in other industries, emphasizing the need for cross-industry knowledge sharing.

Over the years, project management practices have evolved significantly, especially within the software industry. This section explores the historical development of project management practices in the software industry and highlights some key practices that have emerged as industry standards.

In the early days of software development, project management was relatively informal, with a focus on individual efforts rather than coordinated team efforts. However, as software projects became larger and more complex, the need for structured project management practices became evident.

One of the earliest practices to emerge was version control. Version control systems, such as Concurrent Versions System (CVS) and later Subversion (SVN), allowed software teams to track changes to source code and collaborate more effectively. This practice enabled multiple developers to work on the same codebase simultaneously, reducing conflicts and ensuring the integrity of the software.

Another important practice that emerged was the establishment of approval workflows. Software projects require thorough code reviews, testing, and approval processes to ensure the quality of the final product. By implementing approval workflows, project teams could systematically review and validate changes before they were integrated into the software. This practice enhanced collaboration, minimized errors, and improved the overall quality of the project deliverables.

Furthermore, the software industry introduced the concept of deadlines as a means of managing project timelines and ensuring timely delivery. Deadlines served as milestones for progress tracking and helped in prioritizing tasks. They provided a sense of urgency and enabled teams to stay on track throughout the project lifecycle.

Quality control also became a crucial aspect of software project management. Software development practices such as code reviews, automated testing, and continuous integration were adopted to maintain high standards of quality. These practices ensured that software products met the specified requirements, were robust, and delivered a positive user experience.

As the software industry continued to evolve, standardized processes began to emerge. Organizations recognized the value of codifying successful project management practices and making them repeatable across projects. This led to the establishment of frameworks and methodologies such as Agile, Scrum, and Waterfall, which provided a structured approach to managing software projects.

The adoption of software development practices in project management has proven to be beneficial across industries. The software industry's emphasis on collaboration, version control, approval workflows, deadlines, and quality control has provided valuable insights that can be applied to project management in other domains.

In recent years, there has been a growing recognition of the need for cross-industry knowledge sharing. Project managers from various industries are increasingly looking to software development practices as a source of inspiration to enhance their own project management processes. By leveraging software development practices, project managers can streamline workflows, improve collaboration, and achieve better project outcomes.

In conclusion, the evolution of project management practices within the software industry has paved the way for more efficient and effective project management across various domains. The introduction of practices such as version control, approval workflows, deadlines, and quality control has significantly contributed to the success of software projects. By embracing these practices and fostering cross-industry knowledge sharing, project managers can enhance their own project management processes and drive successful project outcomes.

### Benefits of Adopting Software Development Practices
   - Discuss the advantages of incorporating software development practices into project management, including improved efficiency, streamlined workflows, and enhanced collaboration.
   - Highlight the existing tools and reports that software developers have readily available for project managers, which may be underutilized or unknown to non-technical project management professionals.
   - Emphasize the potential for aligning project management processes with established software development practices to achieve better project outcomes.

## Continuous Delivery and Evergreen Documents



---



## Business Document Management

In business, documents are everything. From contract negotiations, to policy manuals, to marketting material, documents are used to communicate with those around us. The production of a published document is an excellent example of a  traditional project in that it proceeds from nothing but an idea, through a process, to produce a tangible object, for example a book.

This process involves a number of checks and balances to ensure that the final product is of sufficient quality:

- authors will write content
- editors will make recommendations to improve readability
- domain experts will be consulted to advise on the accuracy
- researchers will discover or verify evidence to support (or refute) facts presented
- artists will produce cover art
- designers will improve layout and preesentatio of charts or illustrations
- exuctives will approve the statements made in the documents

All of these individuals must be coordinated over a period of time to ensure their efforts are synchronized rather than conflicting, and to optimize their cooperation.

Once the product has moved through its normal workflow, and everyone has had their say, and made their edits, it is finally sent to the printer who will transform all of this intellectual work into a fixed format: a printed document.

Inevitably a mistake is found.

??? Find an example book misprint: https://blog.nzibs.co.nz/proofreading-book-editing/the-embarrassing-spelling-mistake-on-australias-new-50-note/

No matter how hard we try, we are still human and a mistake will be made. If not a mistake, then feedback from our readers will inform us of new information that we did not have before.

## sdlc


When faced with complex informational problems, professionals in Information Technology (IT) often turn to technology to find solutions. If the necessary tools or concepts do not exist, they will invent them. Experimentation, revision, and collaboration are integral to the development process in the IT field.

In the business world, the focus is on establishing a process through consensus and then developing a workflow to enforce it. However, in the software world, especially in open-source communities, the emphasis is on sharing best practices and incorporating them into the development process.

Software developers start by creating software to manage the problem at hand, and then collaborate to modify it according to emerging best practices in the industry. This approach allows developers to establish standardized processes across the entire software industry and refine core tools to meet those expectations.

These practices, such as version control, approval workflows, project deadlines, quality control, and auditable records, are deeply ingrained in the software development industry and are considered assumptions rather than topics of discussion.

The Challenge of Assumptions
The fact that the software industry is built around the assumption of controlled improvement is remarkable and has led to rapid advancements in software development. However, it has also resulted in a siloing of knowledge and practices developed within the industry.

Due to these assumptions, two constraints arise:

Lack of training: There is a lack of training provided to outsiders on the tools used by software developers and how these practices are implemented in their work.
Lack of communication: The industry struggles to clearly communicate to stakeholders, such as project managers, that many elements of project management are already managed by software developers, who possess tools and reports that stakeholders may not be aware of.
This document aims to overcome these constraints and achieve the following objectives:

Help project managers understand how software developers already manage many elements of project management and provide awareness of the existing tools and reports that can aid in their work.
Offer pre-existing tools developed through 30 years of collective experience to organizations, saving them from reinventing the wheel when it comes to releasing their projects.
Business Document Management
In the business world, documents play a vital role in communication. From contract negotiations to policy manuals and marketing materials, documents are used to convey information. The production of a published document follows a project-like process, starting from an idea and progressing through various stages to create a tangible object, such as a book.

This process involves multiple checks and balances to ensure the quality of the final product. Authors write content, editors suggest improvements, domain experts provide advice, researchers verify facts, artists create cover art, designers enhance layout, and executives approve the statements made in the documents. Coordinating these individuals and their efforts over time is crucial to achieving synchronization and optimizing cooperation.

Even with meticulous efforts, mistakes or new information may arise. Similar to software development, where bugs are discovered or new features are required, publishing mistakes can occur, necessitating corrections and updates. The advent of digital distribution has transformed this process, allowing for easier corrections, updates, and immediate content delivery.

Continuous Delivery
Continuous Delivery is a major breakthrough in project management concepts within the software industry. It involves viewing software development from a vertical perspective, where features are implemented in layers, and as soon as a feature is complete enough to be useful, it is released.

This approach offers several benefits:

Faster delivery: Releasing features sooner maintains relevance and addresses the changing needs of customers.
Early feedback: Engaging stakeholders earlier allows for valuable feedback and insights, enabling improvements and adjustments.
Immediate value: By releasing partial features, immediate value is delivered, benefiting stakeholders while work on other sections continues.
This vertical development approach is similar to how business reports are often circulated among interested parties as drafts, seeking feedback and input before finalizing and distributing the official version. This iterative process allows for continuous improvement and ensures that the final document meets the needs and expectations of the stakeholders.

By adopting the principles of continuous delivery in business document management, organizations can streamline their processes, enhance collaboration, and increase efficiency. Here are some key aspects to consider:

Version Control: Implement a version control system for documents, similar to software version control systems like Git. This enables tracking changes, managing different document versions, and facilitating collaboration among multiple contributors.

Collaboration Tools: Utilize collaborative platforms and tools that allow multiple stakeholders to work together on a document simultaneously. These tools provide real-time editing, commenting, and revision tracking features, enhancing communication and coordination.

Important to understand terminology difference so as to avoid "semantic" debates.

Document Automation: Explore automation tools that can streamline repetitive tasks in document creation, formatting, and distribution. Automation can save time and reduce errors, enabling faster document delivery.

Digital Distribution and Updates: Leverage digital platforms for document distribution and updates. Online document repositories or content management systems allow for easy access, distribution, and updating of documents, ensuring stakeholders have the most up-to-date information.

First word is `Information`. This is the technology used to manage information.

The most complex problem IT solves is the development of systems (often software development), the key is those systems are defined textually. Software developers write massive essays.

---

## Target Audience

- Information Stewards
- Frontline to Lower Executive


This document is focused primarily on business teams that are tasked with an act of information stewardship, in particular, this is focused on managers and team leads. While you may not be undertaking many of these activities in your day-to-day operations, rather delegating them out to others, it is hoped that you can gain a holistic view of the tools available to achieve your corporate objectives.

Feynman is famously quoted as having said:

> That which I cannot create, I do not understand. ???

By going through this process, I hope you can gain a strong understanding
of how the parts fit together, ensuring a clear understanding of how these tools can be used to answer difficult questions your organization will be faced with.


## Several tools/concepts developed

WHen you ask someone in Information Technology  to manage an extremly complex informational problem, they are going to reach to the technolgoy to solve that problem. If it doesn't exist, tehey will invent it. They will also experiment and revise over time.

- Version Control
- Approval workflows
- TK???

While these concepts are present in the business world, the focus is on
"agreeing on the process" and then developing a workflow to enforce it.
In the Software world, and in particular the Open Source world, the sense
of sharing best practices is much more at the front of process development.

Rather than developing a process and workflow and then developing software
to enforce it (Share point workflows for example), developers started with
software to manage the problem which they then collaboratively modified
to accommodate best practices that were arising within the industry.

This allowed software developers to gain consensus on standardized processes
across the entire industry, and then refine a few core tools to meet those
refined expectations.

Rather than having discussions about "best practices" like Version Control,
Approval Workflows, Project Deadlines, Quality Control,  or auditable
records, Software Developers have these practices built into the industry
as "assumptions".

### The problem with assumptions

That the entire industry is built around an assumption of controlled improvement,
is amazing, and has allowed Software Development to progress at an extremely
rapid pace. Unfortunately, it has resulted in a siloing of the knowledge
and practices the industry has developed.

Because we assume the practices, we do not

1. design training for the tools for outsiders
1. clearly communicate how those practices are evident in our work

Hopefully this book helps to overcome those two constraints. We want ot share with the business

It is not uncommon for business meetings to take place and have Project
Manager's list all the best practices that must be implemented

- version controlled releases
- signed approvals
- change logs and management

only to have the develoeprs in the room sit quietly. In many cases, it is assumed tha thte developers are sitting quietly becuase they are learning about all the thing that PM-BOK says we must do, however it is actually embarrasment, that it is already being done, and nobody is sure how to expalin that wihtout offending a peer.

1. Help project managers understand how software developers already manage
   many of the elements of project management and likely already have
   tools and reports ready for you that you just may not be aware of.

2. Offer up the pre-existing tools that the past 30 years of collective
   experience have developed to allow organizations to not have to reinvent
   the wheel when it comes time to release their projects.

---

## Business Document Management

In business, documents are everything. From contract negotiations, to policy manuals, to marketting material, documents are used to communicate with those around us. The production of a published document is an excellent example of a  traditional project in that it proceeds from nothing but an idea, through a process, to produce a tangible object, for example a book.

This process involves a number of checks and balances to ensure that the final product is of sufficient quality:

- authors will write content
- editors will make recommendations to improve readiability
- domain experts will be consulted to advise on the accuracy
- researchers will discover or verify evidence to support (or refute) facts presented
- artists will produce cover art
- designers will improve layout and preesentatio of charts or illustrations
- executives will approve the statements made in the documents

All of these individuals must be coordinated over a period of time to ensure their efforts are synchronized rather than conflicting, and to optimize their cooperation.

Once the product has moved through its normal workflow, and everyone has had their say, and made their edits, it is finally sent to the printer who will transform all of this intellectual work into a fixed format: a printed document.

Inevitably a mistake is found.

??? Find an example book misprint: https://blog.nzibs.co.nz/proofreading-book-editing/the-embarrassing-spelling-mistake-on-australias-new-50-note/

No matter how hard we try, we are still human and a mistake will be made. If not a mistake, then feedback from our readers will inform us of new information that we did not have before.

This holds true for software development as much as any business other
document describing a process, and in the early days of computing was
just as costly. Cover art had been printed, disks had been pressed, and
software shipped to storefronts.

This process of feedback and revision is what leads to various editions
of print products: corrections are made, and new information is made available.
Feedback and state changes mean it is not possible to have a perfect print.
The lag between initial writing to distributed consumption means there
will be a desire to change the content shortly after it is published.

In software, this generally meant the shipping of a patch to the software to correct a bug, or add a new feature. In regulatory documentation, books were often kept in binders so that chapters or sections of the book could be updated without printing the entire book again.

Digital distribution has been a game changer in this regard.

It was not long before software developers could release patches to software and post it on a website, but it was not long before they realized that their software itself could check for patches and download them without significant intervention from the user. Web-based publishing reduced the speed of correction even further, given an application published on a website you control, an update can be delivered with the refresh of a webpage.

### Continous Delivery

this lead ot another major breakthrough in the concepts of project management in software: Continous Delivery.

Production of software can be thought of in two ways: vertically or horizontally.

When we think of software being delivered horizontally, we can think of
each technological layer becoming complete before we move on to the next
one. You could think of this as having the entire product constructed,
then sending it for complete validation, and then sending it for complete
editorial review. The problem with this model is that it takes a long
time to see results. Trying to get each layer to perfection before proceeding
to the next one means you **must** have perfect knowledge of your customer's
desires, and you must spend a significant amount of time verifying you
have achieved them.

|         | Feature 1 | Feature 2 | Feature 3 | Feature 4 |
| ---     | :---: | :---: | :---: | :---: |
| Layer 4 |     |     |     |     |
| Layer 3 |     |     |     |     |
| Layer 2 |     |     |     |     |
| Layer 1 |  x  |  x   | x    |  x   |

As we discussed, the longer you take to deliver, the more likely the state
will have changed before you publish.

In business documents, this is not uncommon. A single document will be
written by an individual in totallity, then submitted to a panel of experts
who will all negotiate edits, ten submitted to ... each layer adding more
complexity and time to the delivery.

|           | Preface | Intro | Chap 1 | Chap 2 | Chap ... |
| ---       | :---:   | :---: | :---:  | :---:  | :---:    |
| Publisher |         |       |        |        |          |
| Editor    |         |       |        |        |          |
| Research  |         |       |        |        |          |
| Write     |  x      |  x    |  x     |  x     |  x       |

The other way to look at the development of a publishing artefact is "vertically".
Under these conditions, we look at a feature and determine what we need
to implement at lower layers in order to implement that feature. As soon
as one feature is complete enough to be useful, it is released

|         | Feature 1 | Feature 2 | Feature 3 | Feature 4 |
| ---     | :---:     | :---: | :---: | :---: |
| Layer 4 |  x        |       |     |     |
| Layer 3 |  x        |       |     |     |
| Layer 2 |  x        | x     |     |     |
| Layer 1 |  x        | x     |     |     |

This has three benefits:

1. We can spend less time reaching delivery (maintain relevance)
2. We can engage the other parties sooner (early feedback)
3. We can release valuable content earlier (immediate value)

Again, we see similarities in any report we write at work.

Well before content is delivered to executives for approval, the content
will be circulated to interested parties, not just for review, but because
it is relevant to the work they do. They will want advanced knowledge
of policy changes that will affect them, or be looking to implement new
procedures that can benefit them.

In large organisations, a document will have been well circulated as a
"draft" for years before it is made official. This is thinking vertically.

Rather than waiting for a perfect document, we send the partially completed
chapters to peers to help them immediately, with the understanding that
it is subject to change and revision. In fact, the review and revision
is welcome feedback.

|           | Preface | Intro | Chap 1 | Chap 2 | Chap ... |
| ---       | :---:   | :---: | :---:  | :---:  | :---:    |
| Publisher |         |       |        |        |          |
| Editor    |         |       |  x     |        |          |
| Research  |         |       |  x     |        |          |
| Write     |     x   |       |  x     |        |  x       |

This has the secondary benefit of allowing the author to work on sections
that are particularly inspiring at any given moment:

Writing about the proper procedure? Have a difficult interaction with the
legal department? Maybe now is the time to work on Ch21 about the legal
consequences of failing to follow the procedure. Perhaps you were attending
a child's sports event and are inspired to write the introduction describing
the work/life balance promised by the policy.

Whatever the case, parts can be delivered as they become inspired.


#### Continuous Delivery

- Release Early/Release Often
- Evergreen

While the initial development of a document can be thought of as a major
project with a targetted delivery date, knowing we can release easily
makes it safer for us to release incomplete content.

Rather than waiting for perfection, we can release and appoint a caretaker
for our product. This _steward_'s responsibility will be to make changes
to the existing document as new information becomes available. The point
is that the project doesn't end when the document is published, rather
the artefact continues to live on and will require continuous repair and
maintenance.

- typos will be discovered
- organizational objectives will change
- mistakes will be reported

All of these events will need to be integrated into the next edition of
the document, however, there is no reason this can't be released immediately
as soon as the issue is raised. There are no reason changes cannot be made
continuously.

Using automation and pre-programmed tools, software developers discovered
that they could reduce the cost of a delivery


## Stages of Publishing

Through this book, we will take you through several stages of publishing
an evergreen document using


```{mermaid}
graph LR
	idea --> write
	write --> review --> write
	review --> design
	design --> typeset
	review --> approval --> translate
	translate --> typeset
	typeset --> print
	print --> reader
```

This is not a definitive list, but includes several common stages that a document may go through in going from an idea to bein gplaced in front of a reader.


## Target Audience

- Information Stewards
- Frontline to Lower Executive


This document is focused primarily on business teams that are tasked with an act of information stewardship, in particular, this is focused on managers and team leads. While you may not be undertaking many of these activities in your day-to-day operations, rather delegating them out to others, it is hoped that you can gain an holistic view of the tools available to achieve your corporate objectives.

Feynman is famously quoted for having said:

> That which I cannot create, I do not understand. ???

By going through this process, I hope you can gain a strong understanding of how the parts fit together, ensuring a clear understanding of how these tools can be used to answer difficult questions your organization will be faced with.

---

## Terminology

- `artifacts`: stuff. Things that get created as we do our work. Things
  we may need to keep as evidence

  also: `thing`, `stuff`

- evergreen: a product that is under continuous improvement

- continous improvement: a set of processes that
- stewardship: ???


## Next Steps

```{toctree}
:maxdepth: 2

activity.md
quiz.md
extended.md
lab.md
```
