# Lab - Setup Environment

## Create GitLab account

## Choose Project

There are three themed projects to choose from as you progress:

- Data Stewardship
- Policy Development
- User Manual

The three projects will excercise the same skills, however one likely speaks to your current role and knowledge. It is best you choose the project that will allow you to excercise your own existing knowledge.

All three will operate within a fictitous company that has a new product they are releasing, but will develop a change tracking process for different parts of the company.

### Data Stewardship

You have been flagged as the individual repsonsible for maintaining the accuracy of the corproate ??? dataset. This dataset is very important as it is a reference dataset that forms the backbone of the product your company sells.

As part of your stewardship role, it is your repsonsibility to ensure that the dataset is

1. Available to the public
2. Is updated with the most recent content
3. Suggestions from external consumers need to be integrated
4. Decisions regarding changes must be auditable

As such, you will be publishing a website that diseminates the most recent copy, as well as explanatory notes informing consumers of what the data means.


This project includes special references to:

- Schema.org

### Policy Development

Your small refugee eye clinic has grown, from a small sole proprietorship serving with one person serving many customers, and now serves individuals at multiple facilities across the country. With the growth has come the need ot hire more staff, and with more staff and more customers, comes more complexity.

You have been tasked with maintaining and developing corporate policy that will ensure that employees, managers, and customers understand the boundaries of their roles, responsibilities and interactions. You will need ot take into account the opinions and desires of many stakeholders while maintaining the balanced opinion of the organizaiton.

As part of your stewardship role, it is your repsonsibility to ensure that the policy is

1. Available to the public
2. Is updated with the most recent content
3. Suggestions from customers and staff are integrated
4. Decisions regarding changes must be approved, announced, and justified

As such, you will be publishing a website that diseminates the most recent copy, as well as explanatory notes informing consumers of what the policy means.


### Business Requirements

Your small software development company has scored a golden contract developing archival software for a national library. Suddenly, your small organization must be sure that they have a well documented set of business reuquirements.

As the lead business analyst, your team have been tasked with maintaining and developing the library of those business requirements. This library of requirements will be ever changning and negotiated, but will define the boundaries of roles, responsibilities and interactions. You will need ot take into account the opinions and desires of many stakeholders while maintaining the balanced opinion of the customer while clearly defining it for your peers.

As part of your stewardship role, it is your repsonsibility to ensure that the policy is

1. Available to the public
2. Is updated with the most recent content
3. Suggestions from customers and staff are integrated
4. Decisions regarding changes must be approved, announced, and justified

As such, you will be publishing a website that diseminates the most recent copy, as well as explanatory notes informing consumers of what the policy means.

## Assignment

I'm sure you noticed all the stories share the same theme, deomonstrating te diversity of conidutinos under which these tools are viable.


### Pick a Project

Pick whichever one fits your personal objectives better. The purpose is the same in all cases:

In writing, describe wat your objectives are, how you hope to this will help you in your work and life experiecne. Take the opportunity to refine your thouts.

As we progress through these assignments, we will see that writing itself is a powerful tool to refine your thouhhts. It offers an opporuntity to engage in self-debate, and critical assessment of our own thougts.

Define for yourself why you tink this is a

### Create a Gitlab Account

For the purposes of this class we will be using GitLab. Gitlab is a project managment application that is availab as an Enterprise Edition, Open Source Edition, and offers free online hosting

#### Steps

1. GOTO: [GitLab.com](https://gitlab.com)
2. Click: create an account
3.


### Install VS Codium

Codium is a free software development tool that comes with a number of plugins. We are going to use its built int capabilities to interact with Git based repositories to help us. You will need it installed.

#### Steps

1. NAV: https://codium.com
2. Click: Install
3. Click: plugins
4. Install: Git Pack

### Install TortoiseGit


### Install Notepad++

