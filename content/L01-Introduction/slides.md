
---

Goals

- protection from power imbalances
- transparent decision making

---

PM-BOK

- WOrkflows
- Approvals


---

Many tools/Many Brands

- Microsoft Azure DevOps
- Microsoft GitHub
- GitLab
- Atalassian

All competitors in the same space: Project Management

---

We will use GitLab

- Free (including private projects)
- Free, Enterprise, and OSS editions
-

---

Project Management: can mean something else

PM-BOK

- Beginning, Middle, End
- Budject

---

Project Management: can mean something else

SDLC

- ongoing product lifecycle
- evergreen

Historically called Project Management, may be the same as what PMs call Product Management

Important to understand terminology difference so as to avoid "semantic" debates.

---

## Information Technology

First word is `Information`. This is the technology used to manage information.

The most complex problem IT solves is the development of systems (often, but not always, software development), the key is those systems are defined textually. Software developers write massive essays.

---

## Business Problem

- large documents
- dozens of employees
- work from home

How do you manage a team that large?

---

## Open Source

Created a benefit and a problem

- extremely large documents
- thousands of volunteers
- distributed around the world

How do you manage a team **that** large?


