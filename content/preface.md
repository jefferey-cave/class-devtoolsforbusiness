# Preface

20 years ago, I started work with a small consulting company. Within this 
company, our primary focus was how we could use data and information systems 
to automate the processing of data within businesses.

The work we engaged in was exciting, we could see definitive benefits 
in ensuring that business practices were caried out in a consistent manner, 
data metrics were monitoried and actionable, regulatory requirements were 
safely tracked, and generally businesses could move forward in a calm 
collected manner. Helping a company gain control of thier business practices 
by making their costs more predictable, and therefore creating opportunities 
for reducing those costs, and even in some cases, reducing costs by just 
reducing the cost of managing the processes.

It was amazing to see the transformation within the businesses. Speaking 
to front-line staff, and seeing their change in demeanour from overworked 
and strained, to relaxed and focussed, really showed me how great an impact 
automation and monitoring can have on not just on business, but on the 
health of individuals.

This observation came to a climax during a major upgrade to a piece of 
software I was releasing. I was following the instructions left by the 
previous lead developer and trying to put together a compiled instance 
of our software and really fighting with them, and wondered why we had 
not used an auto-build system. As I worked to build, zip, and package 
the software, I also started to write a script that automated the software.

**Here were were, an organization that developed automated business metric 
gathering and action recommendation (Business Intelligence) and we weren't 
using these principles on ourselves**

It was not long before I began to look at our own processes and practices, 
not just those of our customers. I wanted to see how we could apply the 
very processes we recommended to our customers.

- Change Management
- Accountability
- Automation
- Internal Transparency

I wanted to see how we could "eat our own dog food".

It turned out to be a wonderful time to be exploring these questions. 
A number of automation tools were still around from the late 1960s, but 
new advancements were being made in digital publishing, version control, 
continous integration and continous improvement. Xtreme Development, and 
DevOps were just new ideas.

I spent years applying these tools and practices to our own internal software 
development, and over time began to see significant improvements. Gone 
were the days of storing copies of pre-built software becuase nobody thinks 
we could reproduce the dark magic combination of settings that got it 
to work. Gone were the days of having your collegues overwrite your changes 
because you were both editting the same file at teh same time. Gone were 
the days of relying on the the foibles of humanity to ensure people got 
good service. Instead we had controlled processes with checks and balances 
that removed a lot of the guess work, or at least allowed the business 
to learn from its mistakes and capture those lessons forever.

Basically, we took control of our own destiny and did away with luck. 
Something I think every business would like to acheive.

Over the years, I travelled to many companies and shared some of these 
lessons helping to transform the efficiency and culture of various organizations. 
Over the years these techniques have become compmon place, but I have 
still enjoyed working with companies and students in learning how these 
tools can be applied.

Then I started working with the business side.

I had been foccussed on improving efficiency in the production of software 
for a long time, and it had been almost a decade since I had worked directly 
with the business side to improve efficiency and increase transparency. 
So when I started working with a group of policy and business development 
individuals as an technical advisor, I was shocked when the discussions 
on the best means of enacting Change Management, Information Governance, 
Distributed Work, and Legal Transparency started recommending practices 
that had been discarded and replaced decades ago.

This was a form of Culture Shock: there was no reason I should expect 
other communities to have an understanding of the best practices that 
had evolved around me, but there was no reason I could have predicted 
the difference either.

Having experienced the culture shock, and coming to terms that there was 
no reason for people to have applied these techniques out side of software 
development community, it suddenly dawned on me that people had been deprived 
of a valuable tool that solved many of the problems being discussed in 
business circles. Unfortunately, while many people in the community used 
the tools to develop [process](https://www.w3.org/2015/Talks/1217-github-w3c/#/) 
and [policy](https://www.w3.org/Consortium/Patent-Policy-20200915/), I 
had been unable to see any cases of people carrying thos tools to the 
larger business community. Upon further reflection, I noted that much 
of what had been learned had been passed on implicitely through implicit 
cultural exchange rather than an explicit mechanism: students were picking 
it up from their peers and work placements, rather than through an explicit 
classroom experience.

Something that had revolutionized the way I conducted business, was completely 
unavailable, and resisted as foreign, to those outside the software development 
community.

I was inspired to take a crack at this book, introducing non-technical 
individuals to the tools used by information technologists to manage massive 
collaborative documents.

I hope readers find a set of tools that can be applied within their business 
to achieve their needs, and increase cooperation while doing it. 

I'm curious to see what change you can brring, and I hope you are too.
