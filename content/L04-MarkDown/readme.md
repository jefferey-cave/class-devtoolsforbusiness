# Markdown

```{contents}
:depth: 3
```

Content is King.

In 1994 I got my first contract for a web development project, I was
yoiung, inexpreienced in business, but had been studying a new standard
called HTML (v0.1) in an effort to understand how to deploy websites. A
family member had heard about this, and recommended me as someone that
could build a small company website. I jumped at the chance to explore a
real website for a real company.

Having studied all the theory about websites and what their purposes was,
and how they could be used by a company, I went to meet with the client.

She immediately walked into the board room, slammed down a fat binder and
boldly stated

> I've been doing my homework and I know what we need. I want bouncing
> animated bullets.

I was a little stunned. Really, I had come in with a series of interview
questions about visual design, but mostly with a focus on what types of
information they wanted to share with their customers. They were
completely stunned, and had not considered that.

Eventually, I left with a handful of their standard marketting paper
brochures and an outline of the types of content that would direct
customers to reaching out looking for services and mroe information.

There had been an obvious break down in expectations when I entered the
office: the customer was entirely focussed on what the imagery would look
like, and I was focussed on what information needed to be communicated to
custoemrs. Years later, I was studying SEO practices that could optimizing
search indexing for lookup by Google, and one concept came up continously:

Content is King.

- fashion changes
- algorithms change
- branding canges

... but regardless of changing fasions, we are still trying to comunnicate
the same fundamental information to people. While fasion is important,
catching someone's eye is meaningless without actually communicating
something.

## Advantages of Markdown

### Separating Form and Function

One of the advantages of Markdown is that it intentionally restricts the ability to modify formatting. This limitation forces you to focus on getting the content right instead of being distracted by styling. By separating form and function, Markdown enables faster and more flexible changes without the risk of sacrificing content quality.
### Observation of Changes

With Markdown, you can easily observe and track changes made to the content. The simplicity and readability of Markdown syntax allow for efficient collaboration and version control. By reviewing the differences between document versions, you can evaluate the impact of changes and ensure the integrity of your content.
## Markup Languages

Markdown is just one example of a markup language used for creating structured documents. Other popular markup languages include:

- HTML (Hypertext Markup Language): The foundational language of the web, used for structuring and presenting content on web pages.
- ReStructured Text: A versatile markup language commonly used in technical documentation and documentation generators like Sphinx.

Each markup language has its own syntax and purpose, but Markdown stands out for its simplicity and readability.

## Writing in Plain Text

Markdown allows you to write content in plain text, without the need for complex formatting tools. This approach focuses on the essence of the content rather than its visual presentation. By working in plain text, you can concentrate on the message and meaning, fostering clarity and efficiency in your writing process.



## Markdown Extensions
While Markdown offers a basic set of formatting features, it also provides extensions that enhance its capabilities. These extensions enable additional functionality, such as tables, footnotes, task lists, and syntax highlighting for code blocks. By leveraging these extensions, you can tailor Markdown to suit your specific needs and create more dynamic and interactive content.

## Joining Form and Function

Sphinx is a powerful documentation generation tool that integrates seamlessly with Markdown. It allows you to combine the benefits of Markdown's simplicity with the ability to generate professional-looking documentation. Sphinx provides a robust framework for building documentation projects, facilitating the incorporation of code examples, cross-referencing, and automatic table of contents generation.

## Conclusion


In the world of web development and content creation, the importance of content cannot be overstated. While aesthetics and trends may evolve, delivering valuable and well-structured content remains essential. Markdown, with its focus on content and simplicity, offers a powerful tool for creating meaningful and easily maintainable documentation. By embracing Markdown and understanding its advantages, you can effectively communicate your message and ensure that "Content is King."


## Terminology

- HTML

  Acronym for Hypertext Markup Language, HTML is a standard markup language used for creating structured web pages.

Markdown: Markdown is a lightweight markup language that allows users to write content in plain text format with simple syntax, which is then converted into HTML or other formats for presentation.

ReStructured Text: ReStructured Text is a flexible and extensible markup language commonly used for technical documentation, particularly in projects utilizing Sphinx.

Content: Content refers to the information, text, images, and media elements presented on a website or in a document. It focuses on delivering valuable and meaningful information to the audience.

Form and Function: The concept of separating form and function emphasizes the distinction between content (function) and visual presentation (form). By decoupling the two, content creators can focus on delivering high-quality information without being distracted by formatting concerns.

Sphinx: Sphinx is a powerful documentation generation tool that integrates with various markup languages, including Markdown. It provides a framework for building professional-looking documentation projects with features such as cross-referencing, automatic table of contents generation, and code examples.

Plain Text: Plain text refers to unformatted, standard character-based text without any styling or special formatting. Markdown allows content creation in plain text, enabling a focus on the message and meaning rather than visual presentation.

Markup Languages: Markup languages are languages that use tags or annotations to define the structure and formatting of a document. HTML, Markdown, and ReStructured Text are examples of popular markup languages.

Syntax: Syntax refers to the rules and structure of a programming or markup language. Understanding the syntax of a language is crucial for correctly formatting and composing content.

Extensions: Markdown extensions provide additional functionality beyond the basic Markdown syntax. These extensions enable features such as tables, footnotes, task lists, and syntax highlighting for code blocks, enhancing the capabilities of Markdown.


## Next Steps

```{toctree}
:maxdepth: 2
:caption: Contents

activity.md
quiz.md
extended.md
lab.md
```