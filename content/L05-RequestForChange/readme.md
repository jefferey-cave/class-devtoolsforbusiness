# Request for Change

```{toctree}
:maxdepth: 2
:caption: Contents

activity.md
quiz.md
extended.md
lab.md
```

Different types of content take different lengths of time to produce and require differing levels of co-ooperation. A text message to your best friend takes seconds, a blog post may take a day or two of your focussed effort. Lengthier documents that live a longer period of time are different. The longer the lifespan of content, the more liekly it is to encounter a changing environment (changing its relevancy), or the greater the volume of content the higher the probability of an error being introduced by accident.

To overcome this we create teams of content creators to help produce te initial content, we ahve editors to review its validity, we have lawyers to ensure it conforms to corporate identity, and we still get it wrong, it still changes over time.

Managing content over long periods of time is more about slow edits rather than rapid forced writing. We need to create content, review it, publish it, accept suggestsions for change, then enact those changes in an controlled manner.

We need to enact some form of Governance.

## Submitting Feedback

???


## Reviewing and Prioritizing the Feedback

???


## Using the

???


## Maintenance of Evidence

???


## Conclusion

???

