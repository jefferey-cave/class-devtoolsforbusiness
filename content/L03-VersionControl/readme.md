# Version Control

```{contents}
:depth: 3
```

## A little story

I used to work with a Senior Lead (let's call her Alice) who was pretty brash (I think we all are). At the time, I was just developing the process controls for our small consulting company which included the act of intentionally making changes, reviewing changes, and testing changes. The project Alice and a new hire (let's call him Bob) were working on wasn't going well. The new guy, Bob, kept breaking stuff and being told off by his lead, Alice.

After listening to the tension rise, and hearing another inappropriate chastisement take place. I decided someone needed to step in. If there were mistakes being made, abusive language was not the way to correct them. Bob was an honest guy, publicly dressing him down was inappropriate and downright abusive. Further, the problem seemed odd. We were using a version control system, which logged every change, any questions of incompetence (or implied sabotage) should be very obvious.

I pointed out Alice's group was having trouble and asked if I could help figure out what was going wrong. With the next change, she made I came over to observe, and things became very evident, very quickly.

## Being Mindful

???


## Observing Change

???


## Atomic Commits

???


## Merging

???


## Submitting

???


## A Tool for Mindfulness

One of the key benefits of using version control does not arise from the created record, but rather from the creation of the record.

It is very rare that the log becomes useful. If we assume that we are all
honest actors, trying our best to do what is right, then it we can
honestly stand by our mistakes as mistakes and something to be improved
upon. Having bad actors is actually rare, but finding them is even rarer,
and even through an audit log is very difficult and very unlikely to
happen. While not without value, catching bad actors is of low value.

On the other hand, as an honest actor, we can live various processes to
ensure we keep ourselves honest. We can take actions that will help us to
take honest action even when we are aren't at our best (tired, sick, or
just human). It's under these conditions where we need a tool to help us
hold ourselves accountable.

At the end of a change, when you are ready to share your change with the
world, when you are ready to `commit` ot your change, this si the time to
review that your chnage is what you meant.

1. Look back at what your change was supposed to acheive. Why did you
   start editting.

2. Check the differences between the way the document was, and the way it
   is now. **Check every difference**, is it related to everything else
   you changed, does it achieve what you set out to achieve.

3. Remove any edits that are not related to the current change (perhaps
   move them to a different change)

4. Integrate the changes that others have made. Take the time to ensure
   that your changes are still meaningful given the changes others have
   made. Are you repeating them? Are you contradictining them?

5. Now commit your changes.

## Story Conclusion

1. Alice and Bob figured out what they were going to work on for the week, and then went their separeate ways to do the work.
2. Bob made his assigned changes to the assigned document. In doing so, he noted that a reference to his document was no longer relevant, and made a change in a second document. He ran the basic checks, did an overall review of his work, and submitted them.
3. Meanwhile, Alice was working on her changes. She was very careful to make all her changes and fix all the thigns she could find. Afterward, she ran the basic checks, and did an overall review of her work, and submitted them.

   She received an error stating that there were conflicting changes that someone else had made to her work.

   She was livid. How dare Bob make changes to things she was working on? She was the boss, and she knew that how things were supposed to work. She was making changes to her file, and it was correct, he was obviously just interferring. So she told the machine that her copy was right, his was wrong, and to just use hers and overwrite his.

   Then she submitted.

   Except now it no longer built. The computer was complaining that there were incorrect references in several places ...

   > Bob how have you screwed this up? I had it working an dI know what I' doing. You better fix this before you go home. If you have to work late, so be it. I was told you were smart.

This had been the pattern for a couple of months, slowly escalating to public beratings like this example. Except this time I was observing and intervened.

Looking at the changes in a difference tool, I asked why she had undone Bob's change that fixed the problem.

She hadn't.

I pointed out the change that was highlighted in red (a removal) and her replacement (in green) that undid it.

Well her file was correct.

I pointed out that she herself said there was a problem, and that the change she had applied made te problem. She was starting to sense something might not be right.

She defensively pointed out that she never changed that, she might have copied over it, but she hadn't made a change. Besides, Bob shouldn't be making changes in *her* file.

I called Bob over and asked if he had changed the file initially. He confirmed that he had, and explained that having changed part of his document, the reference no longer worked, so to ensure the integrity of teh overall document he had changed the reference so that it pointed correctly.

Because Alice had not been inspecting the changes she was making, she was undoing other people's valid changes. Had she been inspecting *every* change she was applying, to see that it was correct and fit with her intent, she would have taken te time to understand what was happening.

As the developer of the quality control practices, I ended up requiring that a person inspect *every change* they were checking in, and ensuring it aligned with the comment they wrote, and requiring that a third party review the change for sensibleness (a peer review). If the change did not make sense in the context of the comment, you were required to explain yourself.

While I have been forced to explain myself more than once, this practice immediately put an end to ignoring other people's work... unfortuantley, it was too late to keep Bob from leaving for another job, due to the abuse he'd been taking.

#### Moral

While having version control systems in place is considered a "best practice", just having it present is not the point. The tool exists to facilitate communication and mindfulness, but if you are going to ignore the information that is highlighted by the tool, it may as well not exist.

Tools are only as valuabel as the skill of the person that wields them, the process of using the tools is what makes them useful and conveys the information.

Instead of trying to force your way on others and livign with a very narcisistitic view, try to understand what the actions of others are, and why they are acting the way they are. They probably had purpose and intent behind their actions, assuming you know better, without understanding their purspose is hubris.

Use these tools with intent to overcome teh hubris that lives in all of us if you are going to get value from them.

## Next Steps

```{toctree}
:maxdepth: 2
:caption: Contents

activity.md
quiz.md
extended.md
lab.md
```
