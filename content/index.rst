.. Dev Tools for Business documentation master file, created by
   sphinx-quickstart on Sun Oct 30 20:49:01 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Dev Tools for Business's documentation!
==================================================

By Jefferey Cave

- [GitLab](https://gitlab.com/jefferey-cave/class-devtoolsforbusiness)
- [Read Online](https://jefferey-cave.gitlab.io/class-devtoolsforbusiness/)

.. toctree::
   :maxdepth: 2

   preface.md
   L01-Introduction/readme.md
   L02-Tools/readme.md
   L03-VersionControl/readme.md
   L04-MarkDown/readme.md
   L05-RequestForChange/readme.md
   L06-Branching/readme.md
   L07-Governance/readme.md
   L08-MultipleOutputs/readme.md
   L09-Announcements/readme.md
   L10-Translations/readme.md
   L11-Styling/readme.md
   L12-LawfulStorage/readme.md


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
