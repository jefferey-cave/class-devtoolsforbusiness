<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a>

# Development Tools for Business

Much of my career has been spent introdocuing CI/CD tools to software development teams. Optimizing the automation and auto-pilot management of organziations is not new territory. So I was not totally surprised when I joined a business group as a technical advisor and they were not aware of many of the tools available to manage the problem they were trying to solve.

I was surprised by the depth of the lack of understanding. There were a number of myths and misunderstandings about what the technical concepts were about or capable of. A combinatino of business consultants giving bad technical advice to technical individuals, and an unwillingness to adopt best practices, inspired me to return to the human side of business and share what I learned in 20 years optimizing the production of software.

This essay shares tools and techniques develeoped by software developers for engaging in the prodcution of information prodcuts. Informaion is key to good business, tt is my hope that business can realize gains by looking to information management techniquies developed in the domain of Information technology.

Currently available online.

## Support

Feel free to create an issue if you see a problem with something, or would just like me to expand on a particular topic.

## License

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.

This is a work in progress.

While it is common to share works openly, once complete, this is a work in progress. For now, it is restricively licensed until the best way of sharing it can be determined.

